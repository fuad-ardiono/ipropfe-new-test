const timeout = 50000;
const faker = require('faker');
require('dotenv').config();
const fs = require('fs');

fs.mkdirSync('screenshot/tenant/propertyType')
const propertyTypeScreenshotDir = 'screenshot/tenant/propertyType';


describe('Property Type - Development', () => {
        let page;
        beforeAll(async () => {
            page = await global.__BROWSER__.newPage();
            await page.goto(`http://${process.env.tenant_dev}${process.env.dev_baseurl}/login`);
            await page.setViewport({ width: 1366, height: 800 });

            await page.waitFor(2*1000);

            await page.type('input[name="email"]', process.env.email_dev);
            await page.type('input[name="password"]', process.env.password_dev);

            await page.click('button[type="submit"]');
            await page.waitFor(2*1000);



            let d = new Date();
            let datetimeNow = d.getFullYear() + '-' + d.getDate() + '-' + d.getDate() + '-' + d.getMinutes() + d.getSeconds();
            let imageName = `propertyType-dev-dashboard-${datetimeNow}.png`;

            let imagePath = `${propertyTypeScreenshotDir}/${imageName}`;

            await page.screenshot({
                path: imagePath,
                fullPage: true
            });

            await page.goto(`http://${process.env.tenant_dev}${process.env.dev_baseurl}/properties/types`);
        }, timeout);

        it('should load properties types index page', async () => {
            let text = await page.evaluate(() => document.body.textContent);
            await expect(text).toContain('Add New');
            await expect(text).toContain('Property Types');

            await page.waitFor(2*1000);

            let d = new Date();
            let datetimeNow = d.getFullYear() + '-' + d.getDate() + '-' + d.getDate() + '-' + d.getMinutes() + d.getSeconds();
            let imageName = `propertyType-dev-index-${datetimeNow}.png`;

            let imagePath = `${propertyTypeScreenshotDir}/${imageName}`;

            await page.screenshot({
                path: imagePath,
                fullPage: true
            });
        }, timeout);

        it('should load properties types create page', async() => {
            //create
            await page.click('a[id="addNew"]');
            await page.waitFor(2*1000);
            let text = await page.evaluate(() => document.body.textContent);
            await expect(text).toContain('Create Property Types');

            await page.waitFor(2*1000);

            let d = new Date();
            let datetimeNow = d.getFullYear() + '-' + d.getDate() + '-' + d.getDate() + '-' + d.getMinutes() + d.getSeconds();
            let imageName = `propertyType-dev-create-${datetimeNow}.png`;

            let imagePath = `${propertyTypeScreenshotDir}/${imageName}`;

            await page.screenshot({
                path: imagePath,
                fullPage: true
            });
        }, timeout);

        it('should create properties types and see created properties types in index page', async() => {
            let property_type = 'Apartments';
            await page.click("#property-type");
            await page.keyboard.type(property_type);
            await page.keyboard.press('Enter');
            await page.waitFor(2*1000);
            await page.click('button[type="submit"]');

            await page.waitFor(2*1000);

            await page.type('input[type="text"]', property_type);
            let text = await page.evaluate(() => document.body.textContent);
            await expect(text).toContain(property_type);

            await page.waitFor(2*1000);

            let d = new Date();
            let datetimeNow = d.getFullYear() + '-' + d.getDate() + '-' + d.getDate() + '-' + d.getMinutes() + d.getSeconds();
            let imageName = `propertyType-dev-index-${datetimeNow}.png`;

            let imagePath = `${propertyTypeScreenshotDir}/${imageName}`;

            await page.screenshot({
                path: imagePath,
                fullPage: true
            });
        }, timeout);

        it('should delete last created properties types', async() => {
            //delete
            await page.waitFor(2*1000);
            await page.click('button#delete');
            await page.keyboard.press('Enter');

            await page.waitFor(2*1000);

            let d = new Date();
            let datetimeNow = d.getFullYear() + '-' + d.getDate() + '-' + d.getDate() + '-' + d.getMinutes() + d.getSeconds();
            let imageName = `propertyType-dev-delete-${datetimeNow}.png`;

            let imagePath = `${propertyTypeScreenshotDir}/${imageName}`;

            await page.screenshot({
                path: imagePath,
                fullPage: true
            });
        }, timeout);

        afterAll(async () => {
            await page.close()
        });
    },
);

describe('Property Type - Production', () => {

    },
);


