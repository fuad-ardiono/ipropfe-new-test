const timeout = 50000;
const faker = require('faker');
require('dotenv').config();
const fs = require('fs');

fs.mkdirSync('screenshot/tenant/property')
const propertyTypeScreenshotDir = 'screenshot/tenant/property';


describe('Add Property - Development', () => {
        let page;
        let propertyStreet;
        let propertyType;
        beforeAll(async () => {
            page = await global.__BROWSER__.newPage();

            //do login
            await page.goto(`http://${process.env.tenant_dev}${process.env.dev_baseurl}/login`).then(() => {
                page.setViewport({ width: 1366, height: 800 });
                page.type('input[name="email"]', process.env.email_dev);
                page.type('input[name="password"]', process.env.password_dev);
                page.click('button[type="submit"]');
            });


            //go to properties -> property street menu and create streets
            await page.goto(`http://${process.env.tenant_dev}${process.env.dev_baseurl}/properties/streets`).then(() => {
                page.waitForResponse(response => response.ok());
                page.click('a[id="addNew"]');
                page.waitForResponse(response => response.ok());
                propertyStreet = faker.address.streetName();
                page.type('input[name="property street"]', propertyStreet);
                page.click('button[type="submit"]');
            });


            //go to properties -> property types menu and create property type
            await page.goto(`http://${process.env.tenant_dev}${process.env.dev_baseurl}/properties/types`).then(() => {
                page.click('a[id="addNew"]');
                page.waitForResponse(response => response.ok());
                propertyType = 'Apartments';
                page.click("#property-type");
                page.keyboard.type(propertyType);
                page.keyboard.press('Enter');
                page.waitForResponse(response => response.ok());
                page.click('button[type="submit"]');
            });

        }, timeout);

        it('should load properties create page', async () => {
            //go to properties -> add property menu
            await page.goto(`http://${process.env.tenant_dev}${process.env.dev_baseurl}/properties/create`);
            let text = await page.evaluate(() => document.body.textContent);
            await expect(text).toContain('Create Properties');

            //
            // await page.waitFor(2*1000);
            //
            // let d = new Date();
            // let datetimeNow = d.getFullYear() + '-' + d.getDate() + '-' + d.getDate() + '-' + d.getMinutes() + d.getSeconds();
            // let imageName = `propertyType-dev-index-${datetimeNow}.png`;
            //
            // let imagePath = `${propertyTypeScreenshotDir}/${imageName}`;
            //
            // await page.screenshot({
            //     path: imagePath,
            //     fullPage: true
            // });
        }, timeout);
        //
        // it('should load properties types create page', async() => {
        //     //create
        //     await page.click('a[id="addNew"]');
        //     await page.waitFor(2*1000);
        //     let text = await page.evaluate(() => document.body.textContent);
        //     await expect(text).toContain('Create Property Types');
        //
        //     await page.waitFor(2*1000);
        //
        //     let d = new Date();
        //     let datetimeNow = d.getFullYear() + '-' + d.getDate() + '-' + d.getDate() + '-' + d.getMinutes() + d.getSeconds();
        //     let imageName = `propertyType-dev-create-${datetimeNow}.png`;
        //
        //     let imagePath = `${propertyTypeScreenshotDir}/${imageName}`;
        //
        //     await page.screenshot({
        //         path: imagePath,
        //         fullPage: true
        //     });
        // }, timeout);
        //
        // it('should create properties types and see created properties types in index page', async() => {
        //     let property_type = 'Apartments';
        //     await page.click("#property-type");
        //     await page.keyboard.type(property_type);
        //     await page.keyboard.press('Enter');
        //     await page.waitFor(2*1000);
        //     await page.click('button[type="submit"]');
        //
        //     await page.waitFor(2*1000);
        //
        //     await page.type('input[type="text"]', property_type);
        //     let text = await page.evaluate(() => document.body.textContent);
        //     await expect(text).toContain(property_type);
        //
        //     await page.waitFor(2*1000);
        //
        //     let d = new Date();
        //     let datetimeNow = d.getFullYear() + '-' + d.getDate() + '-' + d.getDate() + '-' + d.getMinutes() + d.getSeconds();
        //     let imageName = `propertyType-dev-index-${datetimeNow}.png`;
        //
        //     let imagePath = `${propertyTypeScreenshotDir}/${imageName}`;
        //
        //     await page.screenshot({
        //         path: imagePath,
        //         fullPage: true
        //     });
        // }, timeout);
        //
        // it('should delete last created properties types', async() => {
        //     //delete
        //     await page.waitFor(2*1000);
        //     await page.click('button#delete');
        //     await page.keyboard.press('Enter');
        //
        //     await page.waitFor(2*1000);
        //
        //     let d = new Date();
        //     let datetimeNow = d.getFullYear() + '-' + d.getDate() + '-' + d.getDate() + '-' + d.getMinutes() + d.getSeconds();
        //     let imageName = `propertyType-dev-delete-${datetimeNow}.png`;
        //
        //     let imagePath = `${propertyTypeScreenshotDir}/${imageName}`;
        //
        //     await page.screenshot({
        //         path: imagePath,
        //         fullPage: true
        //     });
        // }, timeout);

    it('should load properties create page', async () => {
        //delete last inserted property type
        await page.goto(`http://${process.env.tenant_dev}${process.env.dev_baseurl}/properties/types`);
        await page.waitFor(2*1000);
        await page.type('input[type="text"]', propertyType);
        await page.waitFor(2*1000);
        await page.click('button#delete');
        await page.keyboard.press('Enter');

        await page.waitFor(2*1000);

        //delete last inserted street
        await page.goto(`http://${process.env.tenant_dev}${process.env.dev_baseurl}/properties/streets`);
        await page.waitFor(2*1000);
        await page.type('input[type="text"]', propertyStreet);
        await page.waitFor(2*1000);
        await page.click('button#delete');
        await page.keyboard.press('Enter');
    }, timeout);


    afterAll(async () => {
        await page.close()
    });
    },
);

describe('Property Type - Production', () => {

    },
);


