const timeout = 50000;
const faker = require('faker');
require('dotenv').config();
const fs = require('fs');

fs.mkdirSync('screenshot/tenant/propertyStreet');
const propertyStreetScreenshotDir = 'screenshot/tenant/propertyStreet';


describe('Property Street - Development', () => {
        let page;
        beforeAll(async () => {
            page = await global.__BROWSER__.newPage();
            await page.goto(`http://${process.env.tenant_dev}${process.env.dev_baseurl}/login`);
            await page.setViewport({ width: 1366, height: 800 });

            await page.waitFor(2*1000);

            await page.type('input[name="email"]', process.env.email_dev);
            await page.type('input[name="password"]', process.env.password_dev);

            await page.click('button[type="submit"]');
            await page.waitFor(2*1000);



            let d = new Date();
            let datetimeNow = d.getFullYear() + '-' + d.getDate() + '-' + d.getDate() + '-' + d.getMinutes() + d.getSeconds();
            let imageName = `propertyStreet-dev-dashboard-${datetimeNow}.png`;

            let imagePath = `${propertyStreetScreenshotDir}/${imageName}`;

            await page.screenshot({
                path: imagePath,
                fullPage: true
            });

            await page.goto(`http://${process.env.tenant_dev}${process.env.dev_baseurl}/properties/streets`);
        }, timeout);

        it('should load property street index page', async () => {
            let text = await page.evaluate(() => document.body.textContent);
            await expect(text).toContain('Add New');
            await expect(text).toContain('Property Street');

            let d = new Date();
            let datetimeNow = d.getFullYear() + '-' + d.getDate() + '-' + d.getDate() + '-' + d.getMinutes() + d.getSeconds();
            let imageName = `propertyStreet-dev-index-${datetimeNow}.png`;

            let imagePath = `${propertyStreetScreenshotDir}/${imageName}`;

            await page.screenshot({
                path: imagePath,
                fullPage: true
            });
        }, timeout);

        it('should load property street create page', async() => {
            //create
            await page.click('a[id="addNew"]');
            await page.waitFor(2*1000);
            let text = await page.evaluate(() => document.body.textContent);
            await expect(text).toContain('Create Property Streets');

            let d = new Date();
            let datetimeNow = d.getFullYear() + '-' + d.getDate() + '-' + d.getDate() + '-' + d.getMinutes() + d.getSeconds();
            let imageName = `propertyStreet-dev-create-${datetimeNow}.png`;

            let imagePath = `${propertyStreetScreenshotDir}/${imageName}`;

            await page.screenshot({
                path: imagePath,
                fullPage: true
            });
        }, timeout);

        it('should create property street and see created property street in index page', async() => {
            let property_street = faker.address.streetName();
            await page.type('input[name="property street"]', property_street);
            await page.click('button[type="submit"]');
            await page.waitFor(2*1000);
            await page.type('input[type="text"]', property_street);
            let text = await page.evaluate(() => document.body.textContent);
            await expect(text).toContain(property_street);

            let d = new Date();
            let datetimeNow = d.getFullYear() + '-' + d.getDate() + '-' + d.getDate() + '-' + d.getMinutes() + d.getSeconds();
            let imageName = `propertyStreet-dev-index-${datetimeNow}.png`;

            let imagePath = `${propertyStreetScreenshotDir}/${imageName}`;

            await page.screenshot({
                path: imagePath,
                fullPage: true
            });
        }, timeout);

        it('should edit last created property street and see edited property street in index page', async() => {
            //edit
            await page.click('a[id="edit"]');
            await page.waitFor(2*1000);
            const elementHandle=await page.$('input[name="property street"]');
            await elementHandle.click();
            await elementHandle.focus();

            let d = new Date();
            let datetimeNow = d.getFullYear() + '-' + d.getDate() + '-' + d.getDate() + '-' + d.getMinutes() + d.getSeconds();
            let imageName = `propertyStreet-dev-edit-${datetimeNow}.png`;

            let imagePath = `${propertyStreetScreenshotDir}/${imageName}`;

            await page.screenshot({
                path: imagePath,
                fullPage: true
            });

            // click three times to select all
            await elementHandle.click({clickCount: 3});
            await elementHandle.press('Backspace');
            let editPropertyStreet = faker.address.streetName();
            await elementHandle.type(editPropertyStreet);
            await page.click('button[type="submit"]');
            await page.waitFor(2*1000);
            await page.type('input[type="text"]', editPropertyStreet);
            let text = await page.evaluate(() => document.body.textContent);
            await expect(text).toContain(editPropertyStreet);
        }, timeout);

        it('should delete last edited property street', async() => {
            //delete
            await page.waitFor(2*1000);
            await page.click('button#delete');
            await page.keyboard.press('Enter');

            let d = new Date();
            let datetimeNow = d.getFullYear() + '-' + d.getDate() + '-' + d.getDate() + '-' + d.getMinutes() + d.getSeconds();
            let imageName = `propertyStreet-dev-delete-${datetimeNow}.png`;

            let imagePath = `${propertyStreetScreenshotDir}/${imageName}`;

            await page.waitFor(2*1000);

            await page.screenshot({
                path: imagePath,
                fullPage: true
            });
        }, timeout);

        afterAll(async () => {
            await page.close()
        });
    },
);

describe('Property Street - Production', () => {

    },
);


