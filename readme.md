# iProp.io Browser Testing Jest + Puppeteer

## Installation

1.Run this command at your terminal in root project
```
cp env-example .env
```
2.Setup your .env
```
dev_baseurl   => tenant hostname on dev. Example '.web.iprop-dev.com'.
tenant_dev    => tenant name/fqdn on dev. Example 'nana'.
email_dev     => email for login on dev. Example 'example@email.com'.
password_dev  => password for login on dev.

prod_baseurl  => tenant hostname on prod. Example '.iprop.io'.
tenant_prod   => tenant name/fqdn on prod. Example 'nana'.
email_prod    => email for login on prod. Example 'example@email.com'.
password_prod => password for login on prod.

```

##How To Use
1.Run all test
```
npm test
```
2.Run test with pattern
```
node_modules/.bin/jest --testNamePattern "Property Street - Development"
```


## Code Convention
1.Directory Structure
```
/screenshot/tenant/{tenantPage} => location screenshoot file page tenant
/screenshot/global/{globalPage} => location screenshoot global page
/tests/tenant                   => location test file for tenant pages
/tests/global                   => location test file for global pages 
```
2.Code style

Describe development and production
```
describe('Property Street - Development', () => {

    },
);

describe('Property Street - Production', () => {

    },
);

```
Define case with CRUD pattern
```
it('should load property street index page', async () => {
    
}, timeout);

it('should load property street create page', async() => {
    
}, timeout);

it('should create property street and see created property street in index page', async() => {
   
}, timeout);

it('should edit last created property street and see edited property street in index page', async() => {
    
}, timeout);

it('should delete last edited property street', async() => {
   
}, timeout);
```
Dont forget do screenshot every case with format, example : 'propertyType-prod-{locationPage}-YYYY-MM-DD-S.png'.

3.File name
File name example 'propertyType.test.js'.

## Puppeteer Cheatsheet

```js
// di awal buka page, untuk memastikan sudah terload selectornya
await page.waitForSelector('#NIP');

// Isi form text/textarea
await page.type('#NIP','1234567891011');

// klik
await page.click('#bantuanInstansi');

// kolom select option
await page.select('#IDTempatLahir','23');

// Upload file
const profile_picture = await page.$('#profile_picture');
profile_picture.uploadFile('/Users/user/Documents/dummy/rusa.png');

// memanggil fungsi di dalam page
await page.evaluate(() => $('#formInstansiBaru').modal('show'));

await page.waitFor(2*1000);

await page.screenshot({ 
    path: 'screenshots/fungsional.png',
    fullPage: true
});

```